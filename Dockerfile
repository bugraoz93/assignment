FROM python:3.8-alpine

ARG DEBIAN_FRONTEND=noninteractive

COPY requirements.txt ./
RUN pip --no-cache-dir install -r requirements.txt

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

COPY . /assignment/

WORKDIR /assignment
ENV PYTHONPATH "${PYTHONPATH}:/workdir/"

CMD ["python3", "src/main.py"]
