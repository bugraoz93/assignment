# **VISUALFABRIQ Assignment**

**RUN with Docker**


`docker build -t visualfabriq/assignment:bugra .`

`docker run -i visualfabriq/assignment:bugra`

**Sample Inputs**

userID: uid

txnType: debit|credit

date: MM-YYYY
