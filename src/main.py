import calendar
import datetime
import json
import time
from decimal import Decimal
from re import sub

import requests


def get_last_day(date):
    first_day = datetime.datetime(
        year=date.year,
        month=date.month,
        day=1
    ) + datetime.timedelta(microseconds=+1)

    last_day = datetime.datetime(
        year=date.year,
        month=date.month,
        day=calendar.monthrange(date.year, date.month)[1]
    ) + datetime.timedelta(days=1, microseconds=-1)
    return first_day.replace(tzinfo=datetime.timezone.utc), last_day.replace(tzinfo=datetime.timezone.utc)


def get_timestamp_ms(date):
    return int(time.mktime(date.timetuple()) * 1000)


def calculate_criteria(data_dict, average_spending):
    for key in data_dict:
        if data_dict.get(key) > average_spending:
            yield key


def convert_currency_to_decimal(amount):
    return Decimal(sub(r'[^\d.]', '', amount))


def getUserTransaction(uid, txnType, monthYear):
    get_url = "https://jsonmock.hackerrank.com/api/transactions/search"
    params = {"userId": uid}
    response = requests.get(get_url, params)
    response_json = json.loads(response.content)
    all_data = []
    all_data.extend(response_json.get("data"))

    total_page = response_json.get("total_pages")
    page_counter = 1
    while page_counter <= total_page:
        params["page"] = page_counter + 1
        response = requests.get(get_url, params)
        all_data.extend(json.loads(response.content).get("data"))
        page_counter += 1

    split_date = monthYear.split("-")
    first_d, last_d = get_last_day(datetime.date(int(split_date[1]), int(split_date[0]), 1))
    start_date = get_timestamp_ms(first_d)
    end_date = get_timestamp_ms(last_d)

    sum_of_debits = 0

    all_txn_data = {}
    debits = []
    for transaction_data in all_data:
        if start_date <= transaction_data.get("timestamp") <= end_date:
            amount = convert_currency_to_decimal(transaction_data.get("amount"))
            if transaction_data.get("txnType").lower() == "debit":
                sum_of_debits += amount
                debits.append(transaction_data)
            if transaction_data.get("txnType").lower() == txnType.lower():
                all_txn_data[transaction_data.get("id")] = amount

    average = sum_of_debits / len(debits)
    result_list = list(calculate_criteria(all_txn_data, average))

    if len(result_list) == 0:
        return [-1]
    result_list.sort()
    return result_list


if __name__ == '__main__':
    uid = int(input().strip())

    txnType = input()

    monthYear = input()

    result = getUserTransaction(uid, txnType, monthYear)

    print('\n'.join(map(str, result)))
    print('\n')
